// alert("hi!")
const invalidMessage = "Invalid Input."

function isNumber(num1) {
	if (typeof num1 === "number") {
		return true;
	} else {
		alert(invalidMessage);
		return false;
	}
}

function oddEvenChecker (num1) {
	if (isNumber(num1)) {
		const message = "This number is";
		if (num1 % 2 === 1) {
			console.log(`${message} odd.`);
		} else {
			console.log(`${message} even.`);
		}
	}
}

oddEvenChecker(12);
oddEvenChecker(7);
oddEvenChecker("kyla");



function budgetChecker (num1) {
	if (isNumber(num1)) {
		if (num1 > 40000) {
			console.log("You are over the budget.");
		} else {
			console.log("You have resources left.");
		}
	}
}


budgetChecker(50000);
budgetChecker(30000);
budgetChecker("emma");

