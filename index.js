// alert("Hello");

function sample() {
	let x = 1;
	if (x++ === 1) {
		console.log('yes equal');
	}
	console.log(x);
}
sample();

// Basic = 

// Mathematical + - * / %

let num1 = 5;
let num2 = 10;
let num3 = 4;
let num4 = 40;

	// Addition Assignment Operator (+=)
	// left operand
	//right operand
	// num1 = num1 + num4 (re-assigned the value of num1 with the result of num1 + num4)


	num1 += num4;
	console.log(num1);
		// result: 45


	num1 += 55;
	console.log(num1);
		//result: 100 

	console.log(num4);
		//result: 40

	let string1 = "Boston";
	let string2 = "Celtics";
	string1 += string2;
	console.log(string1);
		// result: BostonCeltics
	console.log(string2);

	// Subtraction Assignment Operator (-=)

	num1 -= num2;
	console.log(num1);
		//result: 90

	num1 -= num4
	console.log(num1);
		//result: 50 

	num1 -= 10; 
	console.log(num1);

	num1 -= string1
	console.log(num1);
		//NaN --not a number

	string1 -= string2
	console.log(string1);
		//NaN

	// Multiplication Assignment Operator (*=)
	num2 *= num3;
	console.log(num2);

	num2 *= 5;
	console.log(num2);

	// Division Assignment Operator (/=)
	num4 /= num3;
	console.log(num4);
		//result: 10

	num4 /= 2;
	console.log(num4);
		//result: 5

//MODULO --to check if there is a remainder

	console.log(num2);
		//result: 5 
	console.log(num4);
		//result: 200

	console.log(num4 % num2)
		//result: 

let y = 15;
let x = 2;

console.log(y % x);




// Mathematical Operations: MDAS 

let mdasResult = 1 + 2 - 3 * 4 / 5
console.log(mdasResult);
 //result: 0.6


// PEMDAS - Parenthesis, Exponent, MDAS

let pemdasResult = 1 + (2 - 3) * (4 / 5);
console.log(pemdasResult);
	//result: 0.2


// Increment and Decrement

	// 2 Kinds: pre-fix and post-fix

	let z = 1;
	++z;
	console.log(z);
		//result: 2
		// the value of z was added with 1 and is immediately returned

	z++; 
	console.log(z);
		//result: 3 --the value of z is added with 1
	console.log(z++);
		//result: 3  --the previous value of the variable is returned first before increment
	console.log(z);
		//result: 4 --the new value is returned

	console.log(--z);
		//result: 3 --the reult subtracted by 1 is returned immediately
	console.log(z--);
		//result: 3 --returned the previous value
	console.log(z);
		//result: 2 --returned the new value


// Comparison Operators -compares l and r values; returns a boolean (t or f)
// Equality or Loose Equality (==)

console.log(1==1);
	//result: 1

let isSame = 55 == 55;
console.log(isSame);
	//true

console.log(1 == "1");
	// true
	// note: sameness over value (forced coercion over comparison)

console.log(0 == false);
	// true
	//false is converted to zero
console.log(1 == true);
	//true
	// true is equal to 1
console.log("1" == 1);
	//true
console.log(true == "true");
	//false
	// note: "true" is a string, the value of true is 1 

/*
	With loose...notes
*/

// Strict Equality Operator
console.log(true === "1");
	//false
	// note: checks both the value and the data type
	// no type coercion

console.log("Lisa" === "lisa");
	//false


// Inequality Operators : loose (!=) and strict
	
	// LOOSE INEQUALITY (!=)

	console.log("1" != 1);
	//false 

	console.log("Rose" != "Jenny")
	//true

	console.log(false != 0);
	//false

	console.log(true != "true")
	//true


	// STRICT INEQUALITY !==

	console.log("5" !== 5);
	//true

	console.log(5 !== 5)
	//false

	console.log("true" !== true)
	//true


// mini activity

let name1 = "Juan";
let name2 = "Shane";
let name3 = "Peter";
let name4 = "Jack";

let number = 50;
let number2 = 60;
let numString1 ="50";
let numString2 ="60";

console.log(numString1 == number);
	//true
console.log(numString1 === number);
	//false
console.log(numString1 != number);
	//false
console.log(name4 !== name3);
	//true
console.log(name1 == "juan");
	//false
console.log(name1 === "Juan");
	//true


//Relational Comparison Operators - checks rel between operands (  >  <  >=  <=  )

let q = 500 
let r = 700
let w = 8000 
let numString3 = "5500";

console.log(q > r);
	//false

console.log(w > r);
	//true

console.log(w < q);
	//false

console.log(q < 1000);
	//true

console.log(numString3 < 6000);
	//true

console.log(numString3 < "Jose");
 	//true
 	// note: ERRATIC

 console.log(w >= 8000);
 	//true

 console.log(r >= q);
 	//true

 console.log(q <= r);
 	//true

 console.log(w <= q);
 	//false


// Logical Operators: &&   ||

	//And Operator (&&) - both/all operands added must be true or will result to true

	let isAdmin = false;
	let isRegistered = true; 
	let isLegalAge = true;

	let authorization1 = isAdmin && isRegistered; 
	console.log(authorization1);
		//fasle

	let authorization2 = isLegalAge && isRegistered;
	console.log(authorization2);
		//true

	let requiredLevel = 95;
	let requiredAge = 18; 

	let authorization3 = isRegistered && requiredLevel === 25; 
	console.log(authorization3);
		//false 

	let authorization4 = isRegistered && isLegalAge && requiredLevel ===95;
	console.log(authorization4);
		//true

	let userName = "gamer2001"
	let userName2 = "shadow1991"
	let userAge = 15
	let userAge2 = 30

	let registration1 = userName.length > 8 && userAge >= requiredAge;
	console.log(registration1);
		//false


	let registration2 = userName2.length > 8 && userAge2 >= requiredAge; 
	console.log(registration2);
		//true


	let userLevel1 = 100;
	let userLevel2 = 200;

	let guildRequirement1 = isRegistered && userLevel1 >= requiredLevel && userAge >= requiredAge;
	console.log(guildRequirement1);
		//false



	// OR Operator (|| - double pipe) - true if atleast 1 of the operands is true

	let guildRequirement2 = isRegistered || userLevel2 >= requiredLevel || userAge2 >= requiredAge;
	console.log(guildRequirement2);
		//true

	let guildAdmin = isAdmin || userLevel2 >= requiredLevel
	console.log(guildAdmin);
		//false


	// NOT Operator (!) -turns boolean value into the opposite direction

	console.log(!isRegistered);
		//false

	console.log(!guildAdmin);
		//true


	// If-else statements
/*
	if (true) {
		alert("We run an If Condition!")
	}
*/

// IF STATEMENT - TRUE

let userName3 = "crusader_1993";
let userLevel3 = 25; 
let userAge3 = 20;

if (userName3.length > 10) {
	console.log("Welcome to Game Online!")
};

if (userLevel3 >= requiredLevel) {
	console.log("You are qualified to join the guild!")
};

if (userName3 >= 10 && isRegistered && isAdmin) {
	console.log("Thank you for joining the Admin!")
}


// ELSE STATEMENT -FALSE

if (userName3 >= 10 && userLevel3 <= 25 && userAge3 >= requiredAge) {
	console.log("Thank you for joining the Noob guild!")
} else {
	console.log("You are too strong to be a noob")
}


// else-if 
	// executes a statement if the previous or original condition is false
	// but another specified condition resulted to true

if (userName3.length >= 10 && userLevel3 <= 25 && userAge3 >= requiredAge) {
	console.log("Thank you for joining the Noob guild")
} else if (userLevel3 > 25) {
	console.log("You are too strong to be a noob")
} else if (userAge3 < requiredAge) {
	console.log("You are too young to join the noob");
}


// if - else inside a function

function addNum (num1, num2) {
	if (typeof num1 === "number" && typeof num2 === "number") {
		console.log("Run only if both arguments passed are number types");
		console.log(num1 + num2);	
	} else {
		console.log("One of the arguments are not numbers");
		};	
};

addNum (5, 2);



function login (username, password) {
	if (typeof username=== "string" && typeof password=== "string") {
		console.log("Both arguments are string");

		//nested if-else
		// will run if the parent statement is able to agree to accomplish its condition
		if (username.length >= 8 && password.length >= 8){
			alert("Thank you for logging in");
		} else if (username.length < 8) {
			alert("username too short");
		} else if (password.length < 8) {
			alert("password is too short");
		}
	} else {
		console.log("One of the arguments is not string type");
	}
};

login ("theTinker", "tinkerbell");

	




// Swith Statements - alternative to if, else-if, else tree
/*
	Syntax:
		switch(expression/ condition) {
			case value:
				statement;
				break;
			default:
				statement;
				break;
		}
*/


let hero = "Jose Rizal";

switch(hero) {
	case "Jose Rizal":
	console.log("National Hero of the Philippines");
	break;

	case "George Washington":
	console.log("Hero of American Revolution");

	case "Hercules":
	console.log("Legendary Hero of the Greeks");
}

function roleChecker(role){

	switch(role){
		case "Admin":
			console.log("Welcome to Admin!")
			break;
		case "User":
			console.log("Welcome User!")
			break;
		case "Guest":
			console.log("Welcome Guest!")
			break;
		default:
			console.log("Invalid role");
	}
};

roleChecker("Guest");


// Function with if-else and return keyword

function gradeEvaluator(grade) {

	if (grade >= 90){
		return "A"
	} else if (grade >= 80) {
		return "B"
	} else if (grade >= 71) {
		return "C"
	} else if (grade <= 70) {
		return "F"
	} else {
		return "invalid grade"
	}
};

let letterDistinction = gradeEvaluator(85);
console.log(letterDistinction);

// TERNARY Operator - short hand for if-else
		// 	-not meant for complex if-else trees
/*
	Syntax:
		Condition ? if-statement : else-statement
*/

let price = 5000; 

price > 1000 ? console.log("Price isa over 1000") : console.log("Price is less than 1000")

/* ternary operator needs else-statement

villain === "Harvey Dent" ? console.log("Yo were supposed to be the chosen one") */

let villain = "Harvey Dent";

villain === "Two Face"
? console.log("You lived long enough to be a villain")
: console.log("Not quite villainous yet")


let robin1 = "Dick Grayson";
let currentRobin = "Tim Drake";

let isFirstRobin = currentRobin === robin1 ? true : false;
console.log(isFirstRobin);


let a = 7 

a === 5 
? console.log("A")
: console.log(a === 10 ? console.log("A is 10"):
	console.log ("A is not 5 or 10"));

let conditionA = true;
let conditionB = false;
let conditionA = true;
let result = !(conditionA && (conditionB || conditionC));

console.log(result)
